### -*-Makefile-*- pour préparer "Programmation lettrée et R Markdown"
##
## Copyright (C) 2018 Vincent Goulet
##
## 'make html' crée les diapos en format HTML avec R Markdown.
##
## 'make pdf' crée les diapos en format PDF avec R Markdown.
##
## 'make contrib' crée le fichier COLLABORATEURS.
##
## 'make zip' crée la distribution du matériel pédagogique.
##
## 'make release' crée une nouvelle version dans GitLab et téléverse
## le fichier .zip.
##
## 'make all' est équivalent à 'make html' question d'éviter les
## publications accidentelles.
##
## Auteur: Vincent Goulet
##
## Ce fichier fait partie du projet
## "Programmation lettrée et R Markdown"
## https://gitlab.com/vigou3/laboratoire-rmarkdown


## Principaux fichiers
MASTER = laboratoire-rmarkdown.html
ARCHIVE = ${MASTER:.html=.zip}
README = README.md
NEWS = NEWS
COLLABORATEURS = COLLABORATEURS
LICENSE = LICENSE

## Autres fichiers à inclure dans l'archive
CONTRIBUTING = CONTRIBUTING.md

## Fichiers des exercices
EXERCISES = $(wildcard exercices/*.md) $(wildcard exercices/*.Rmd)

## Informations de publication extraites du fichier maître
TITLE = $(shell awk -F\" '/^title:/ { print $$2; exit }' ${MASTER:.html=.Rmd})
REPOSURL = $(shell awk '/^  reposurl:/ { print $$2; exit }' ${MASTER:.html=.Rmd})
YEAR = $(shell awk -F\" '/^  year:/ { print $$2; exit }' ${MASTER:.html=.Rmd})
MONTH = $(shell awk -F\" '/^  month:/ { print $$2; exit }' ${MASTER:.html=.Rmd})
VERSION = ${YEAR}.${MONTH}

## Auteurs à exclure du fichier COLLABORATEURS (regex)
OMITAUTHORS = Vincent Goulet|Inconnu|unknown|davebulaval

## Outils de travail
RSCRIPT = Rscript -e
CP = cp -p
RM = rm -rf

## Dossier temporaire pour construire l'archive
BUILDDIR = builddir

## Dépôt GitLab et authentification
REPOSNAME = $(shell basename ${REPOSURL})
APIURL = https://gitlab.com/api/v4/projects/vigou3%2F${REPOSNAME}
OAUTHTOKEN = $(shell cat ~/.gitlab/token)

## Variables automatiques
TAGNAME = v${VERSION}


all: html

.PHONY: html zip release upload create-release publish clean

FORCE: ;

${MASTER}: ${MASTER:.html=.Rmd} $(wildcard images/*)
	${RSCRIPT} 'rmarkdown::render("${MASTER:.html=.Rmd}", \
	                              output_format = "ioslides_presentation")'

${MASTER:.html=.pdf}: ${MASTER:.html=.Rmd} $(wildcard images/*)
	${RSCRIPT} 'rmarkdown::render("${MASTER:.html=.Rmd}", \
	                              output_format = "beamer_presentation")'

${COLLABORATEURS}: FORCE
	(git log --pretty="%an%n" && echo "Laurent Caron") | sort | uniq | \
	  grep -v -E "${OMITAUTHORS}" | \
	  awk 'BEGIN { print "Les personnes dont le nom [1] apparait ci-dessous ont contribué à\nl'\''amélioration de «${TITLE}»." } \
	       { print $$0 } \
	       END { print "\n[1] Noms tels qu'\''ils figurent dans le journal du dépôt Git\n    ${REPOSURL}" }' > ${COLLABORATEURS}

html: ${MASTER}

pdf: ${MASTER:.html=.pdf}

contrib: ${COLLABORATEURS}

release: zip check upload create-release publish

zip: ${MASTER} ${MASTER:.html=.pdf} ${README} ${NEWS} ${EXERCISES} ${LICENSE} ${COLLABORATEURS} ${CONTRIBUTING}
	if [ -d ${BUILDDIR} ]; then ${RM} ${BUILDDIR}; fi
	mkdir -p ${BUILDDIR}/exercices
	touch ${BUILDDIR}/${README} && \
	  awk 'state==0 && /^# / { state=1 }; \
	       /^## Auteur/ { printf("## Édition\n\n%s\n\n", "${VERSION}") } \
	       state' ${README} >> ${BUILDDIR}/${README}
	${CP} ${MASTER} ${MASTER:.html=.pdf} ${NEWS} ${LICENSE} \
	      ${COLLABORATEURS} ${CONTRIBUTING} \
	      ${BUILDDIR}
	${CP} ${EXERCISES} ${BUILDDIR}/exercices
	cd ${BUILDDIR} && zip --filesync -r ../${ARCHIVE} *
	if [ -e ${COLLABORATEURS} ]; then ${RM} ${COLLABORATEURS}; fi
	${RM} ${BUILDDIR} 

check:
	@echo ----- Checking status of working directory...
	@if [ "master" != $(shell git branch --list | grep ^* | cut -d " " -f 2-) ]; then \
	     echo "not on branch master"; exit 2; fi
	@if [ -n "$(shell git status --porcelain | grep -v '^??')" ]; then \
	     echo "uncommitted changes in repository; not creating release"; exit 2; fi
	@if [ -n "$(shell git log origin/master..HEAD)" ]; then \
	    echo "unpushed commits in repository; pushing to origin"; \
	     git push; fi

upload :
	@echo ----- Uploading archive to GitLab...
	$(eval upload_url_markdown=$(shell curl --form "file=@${ARCHIVE}" \
	                                        --header "PRIVATE-TOKEN: ${OAUTHTOKEN}"	\
	                                        --silent \
	                                        ${APIURL}/uploads \
	                                   | awk -F '"' '{ print $$12 }'))
	@echo Markdown ready url to file:
	@echo "${upload_url_markdown}"
	@echo ----- Done uploading files

create-release :
	@echo ----- Creating release on GitLab...
	if [ -e relnotes.in ]; then rm relnotes.in; fi
	touch relnotes.in
	$(eval FILESIZE = $(shell du -h ${ARCHIVE} | cut -f1 | sed 's/\([KMG]\)/ \1o/'))
	awk 'BEGIN { ORS = " "; print "{\"tag_name\": \"${TAGNAME}\"," } \
	      /^$$/ { next } \
	      (state == 0) && /^# / { \
		state = 1; \
		out = $$2; \
	        for(i = 3; i <= NF; i++) { out = out" "$$i }; \
	        printf "\"description\": \"# Édition %s\\n", out; \
	        next } \
	      (state == 1) && /^# / { exit } \
	      state == 1 { printf "%s\\n", $$0 } \
	      END { print "\\n## Télécharger la distribution\\n${upload_url_markdown} (${FILESIZE})\"}" }' \
	     ${NEWS} >> relnotes.in
	curl --request POST \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     "${APIURL}/repository/tags?tag_name=${TAGNAME}&ref=master"
	curl --data @relnotes.in \
	     --header "PRIVATE-TOKEN: ${OAUTHTOKEN}" \
	     --header "Content-Type: application/json" \
	     ${APIURL}/repository/tags/${TAGNAME}/release
	${RM} relnotes.in
	@echo ----- Done creating the release

publish:
	@echo ----- Publishing the slides...
	git checkout pages && \
	  ${MAKE} && \
	  git checkout master
	@echo ----- Done publishing

clean:
	${RM} ${MASTER} \
	      ${MASTER:.html=.pdf} \
	      ${ARCHIVE} \
	      ${COLLABORATEURS} \
	      *.aux *.log *.snm *.nav *.vrb
